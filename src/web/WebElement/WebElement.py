from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from src.common.exceptions.Exceptions import UnknowLocatorException,\
                                                ElementNotFound, \
                                                TextDoesntMatch, \
                                                ColorDoesntMatch, \
                                                TextWasFound
from src.common.constants import TypeLocator
from src.common.settings.setting_wait import wait_element, wait_element_text
from src.web.WebElement.WaitWebPage import WaitWebPage
import time
"""
Модуль содержит классы кастомных веб-элементов.
"""
class WebElement(object):
    """
    Базовый веб-элемент. Реализует все типичные действия достпные веб-компонентов, синхронизирует драйвер и веб-страницу.
    Класс любого веб-элемента будет наследоваться от WebElement.
    """
    def __init__(self, driver, locator, locator_type):
        self.driver = driver
        self.locator = locator
        self.locator_type = locator_type
        self.element = None
        self.list_elements = None
        self.wait = WebDriverWait(self.driver, wait_element)
        self._wait_loading_page()
        self._locate_elements()

    def _locate_elements(self):
        if self.locator_type == TypeLocator.XPATH:
            self._locate_elements_by_xpath()
        elif self.locator_type == TypeLocator.CSS:
            self._locate_elements_by_css_selector()
        else:
            raise UnknowLocatorException(self.locator_type)

    def _locate_elements_by_css_selector(self):
        try:
            self.wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, self.locator)))
        except TimeoutException:
            raise ElementNotFound(self.locator, self.locator_type)
        self.element = self.driver.find_element_by_css_selector(self.locator)
        self.list_elements = self.driver.find_elements_by_css_selector(self.locator)

    def _locate_elements_by_xpath(self):
        try:
            self.wait.until(EC.presence_of_all_elements_located((By.XPATH, self.locator)))
        except TimeoutException:
            raise ElementNotFound(self.locator, self.locator_type)
        self.element = self.driver.find_element_by_xpath(self.locator)
        self.list_elements = self.driver.find_elements_by_xpath(self.locator)

    def _wait_loading_page(self):
        """
        Выполнить последовательность действий для синхрозиации драйвера и веб-страницы.
        :return:
        """
        WaitWebPage.wait_web_page(self.driver)

    def wait_text(method):
        """
        Позволяет повторить проверку, если она не удалась (например, если элемент не загрузился до конца).
        :return:
        """
        def repeater(self, *a, **kw):
            result = None
            for iter in range(wait_element_text*2):
                try:
                    if a and kw:
                        result = method(self, *a, *kw)
                    elif a:
                        result = method(self, *a)
                    elif kw:
                        result = method(self, *kw)
                    else:
                        RuntimeError("Что-то странное передано в wait_text: {}, {}".format(a, kw))
                except (AssertionError, TextDoesntMatch, StaleElementReferenceException):
                    # обновить элементы, если на странице что-то изменилось
                    self._locate_elements()
                    time.sleep(0.5)
                else:
                    break
            return result
        return repeater

    def get_text(self):
        return self.element.text

    def get_texts(self):
        """ Получить список текстов из всех элементов найденный по локатору"""
        return [element.text for element in self.list_elements]

    def get_html(self):
        return self.element.get_attribute("outerHTML")

    @wait_text
    def check_text(self, text, text_sync=None):
        """
        Проверить текст элемента на равенство text. Опиционально передать text_sync -- текст который должен быть
        загружен на странице, чтобы с ней можно было продолжать взаимодействие.
        :param text: текст для сверки с текстом элемента.
        :param text_sync: текст, который надо "подождать". Если не передан, то используется text.
        :return:
        """
        element_text = self.get_text()
        # Убедится, что на странице вообще есть такой текст, и элемент содержащий этот текст готов к работе.
        WaitWebPage.locate_text(self.driver, text) if not text_sync else WaitWebPage.locate_text(self.driver, text_sync)
        try:
            assert (element_text == text)
        except AssertionError:
            raise TextDoesntMatch(element_text, text)

    def check_substring(self, text, text_sync=None):
        """ Aналогично check_text, но проверяет наличие подстроки в тексте элемента, а не полное совпадение."""
        element_text = self.get_text()
        WaitWebPage.locate_text(self.driver, text) if not text_sync else WaitWebPage.locate_text(self.driver, text_sync)
        try:
            assert (element_text in text)
        except AssertionError:
            raise TextDoesntMatch(element_text, text)

    def uncheck_substring(self, substring, text_sync=None):
        """ Проверить, что в тексте элемента не встречается подстрока."""
        element_text = self.get_text()
        WaitWebPage.locate_text(self.driver, substring) if not text_sync else WaitWebPage.locate_text(self.driver, text_sync)
        try:
            assert (substring not in element_text)
        except AssertionError:
            raise TextWasFound(element_text, substring)

    def uncheck_substring_everywhere(self, substring, text_sync=None):
        """То же самое, что и uncheck_substring, но выполняется для каждого элемента найденого по локатору."""
        element_texts = self.get_texts()
        WaitWebPage.locate_text(self.driver, substring) if not text_sync else WaitWebPage.locate_text(self.driver, text_sync)
        try:
            for element_text in element_texts:
                assert (substring not in element_text)
        except AssertionError:
            raise TextWasFound(element_text, substring)

    def check_color(self, color, css_property="color"):
        """
        Проверить, что цвет элемента равен переданому.
        Цвет элемента получить из свойства css_property.
        :param color: цвет, в формате #FFFFF
        :param css_property: css свойство из которого нужно получить цвет.
        """
        rgba = self.element.value_of_css_property(css_property)
        r, g, b, *a = rgba.strip('rgba').replace(')','').replace('(', '').split(',')
        color_of_element = '#%02x%02x%02x' % (int(r), int(g), int(b))
        color_of_element = color_of_element.upper()
        try:
            assert (color == color_of_element)
        except AssertionError:
            raise ColorDoesntMatch(color_of_element, color, css_property)

    def click(self):
        self.element.click()

    def send_keys(self, keys):
        self.element.send_keys(keys)

    def clear(self):
        self.element.clear()


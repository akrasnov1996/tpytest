from src.common.settings.setting_wait import wait_element, no_element
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from src.common.exceptions.Exceptions import ElementWasFound, ElementNotFound
import time

class WaitWebPage(object):
    """
    Содержит методы для синхронизации драйвера и веб-страницы, и для проверок состояния элементов.
    """

    @staticmethod
    def wait_web_page(driver):
        """
        Синхрозировать драйвер и веб-страницу:
        1. Дождаться исчезновения прогресс-бара
        2. Дождаться исчезновения анимации загрузки
        3. Дождаться загрузки страницы
        :param driver:
        :return:
        """
        driver.execute_script('onload=myFunction();function myFunction() {return 0;}')
        wait = WebDriverWait(driver, wait_element)
        try:
            wait.until_not(EC.presence_of_element_located((By.XPATH, '//div[@class="ui-loader-round"]')))
            wait.until_not(EC.visibility_of_element_located((By.XPATH, "//div[@class = '_2x63V']")))
        except TimeoutException:
            raise ElementWasFound("Прогресс бар крутится слишком долго..")


    @staticmethod
    def not_locate_text(driver, text):
        """проверить, что переданный текст, не отображается."""
        wait = WebDriverWait(driver, no_element)
        try:
            wait.until_not(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), '{}')]".format(text))))
        except TimeoutException:
            raise ElementWasFound("элемент с текстом '{}'".format(text))

    @staticmethod
    def locate_text(driver, text):
        """дождаться появления текста на странице"""
        wait = WebDriverWait(driver, no_element)
        xpath = "//*[contains(text(), '{}')]".format(text)
        try:
            wait.until(EC.presence_of_all_elements_located((By.XPATH, xpath)))
        except TimeoutException:
            raise ElementNotFound(xpath, By.XPATH)

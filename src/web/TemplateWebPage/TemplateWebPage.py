from src.common.constants import TypeLocator
from src.common.utils.utils import compare_text
from src.web.WebElement.WebElement import WebElement
from src.web.WebElement.WaitWebPage import WaitWebPage

"""
Описание шаблонов веб-страниц.
"""
class TemplateWebPage(object):
    """
    Шаблон для создания веб-страницы.
    Описывает способы взаимодействия с компонентами, которые есть на любой странице www.tinkoff.ru.
    """
    def __init__(self, driver):
        self.driver = driver

    def __payments_link(self):
        return WebElement(self.driver, '//a[@href = "/payments/"]', TypeLocator.XPATH)

    def click_payments(self):
        self.__payments_link().click()

    def get_page_source(self):
        return self.driver.page_source

    def compare_text(self, text_1, text_2):
        compare_text(text_1, text_2)

    def not_locate_text(self, text):
        WaitWebPage.not_locate_text(self.driver, text)
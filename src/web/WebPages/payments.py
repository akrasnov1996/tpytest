from src.common.constants import TypeLocator
from src.web.WebElement.WebElement import WebElement
from src.web.TemplateWebPage.TemplateWebPage import TemplateWebPage
import time


class Payments(TemplateWebPage):
    """
    Класс описываюший страницы:
    https://www.tinkoff.ru/payments/
    https://www.tinkoff.ru/payments/kommunalnie-platezhi/
    """
    def __init__(self, driver):
        super().__init__(driver)

# ------------ * Методы для создания компонентов страницы * ------------ #

    def __communal_payments_link(self):
        return WebElement(self.driver, '//a[@href = "/payments/kommunalnie-platezhi/"]', TypeLocator.XPATH)

    def __label_region(self):
        return WebElement(self.driver, '//h1[@class = "ui-title ui-title_center payment-page__title"]', TypeLocator.XPATH)

    def __region_block(self):
        return WebElement(self.driver, '//span[@class = "ui-link payment-page__title_inner"]', TypeLocator.XPATH)

    def __label_region_in_region_block(self):
        return WebElement(self.driver, '//h3[@class = "ui-title ui-title_center ui-regions__title"]', TypeLocator.XPATH)

    def __active_region_in_region_block(self):
        return WebElement(self.driver, '//span[@class = "ui-link ui-link_active"]', TypeLocator.XPATH)

    def __button_close_region_block(self):
        return WebElement(self.driver, '//span[@class = "ui-icon ui-icon_border ui-icon_size_25 ui-icon_color_dorblue ui-modal__close"]', TypeLocator.XPATH)

    def __first_provider(self):
        return WebElement(self.driver, '(//a[contains(@class, "ui-link ui-menu__link ui-menu__link_logo")]/span[@class = "ui-link__text"])[1]', TypeLocator.XPATH)

    def __filter_region(self):
        return WebElement(self.driver, '//div[@class= "ui-input ui-regions__input ui-input_fade"]//input', TypeLocator.XPATH)

    def __region_contains_text(self, text):
        """
        Создать объект контейнера региона, содержащией переданный текст.
        :param text: текст, который должен быть в контейнере региона.
        :return:
        """
        return WebElement(self.driver, '//div[@class= "ui-regions__item"]//span[contains(text(), "{}")]'.format(text), TypeLocator.XPATH)

    def __quick_search(self):
        return WebElement(self.driver, '//input[@class = "ui-search-input__input"]', TypeLocator.XPATH)

    def __first_provider_after_search(self):
        return WebElement(self.driver, '//div[@class="ui-search-flat__title-box"]', TypeLocator.XPATH)

    def __list_providers(self):
        return WebElement(self.driver, '//ul[@class="ui-menu ui-menu_icons"]//span[@class="ui-link__text"]', TypeLocator.XPATH)

# ------------ * Методы для взаимодействия со страницей * ------------ #

    def uncheck_all_providers(self, text):
        self.__list_providers().uncheck_substring_everywhere(text)

    def enter_filter(self, text):
        self.__quick_search().send_keys(text)

    def check_first_provider(self, provider):
        self.__first_provider().check_text(provider)

    def check_first_provider_after_search(self, provider):
        self.__first_provider_after_search().check_text(provider)

    def get_first_provider(self):
        return self.__first_provider().get_text()

    def click_link_first_provider(self):
        self.__first_provider().click()

    def click_link_first_provider_after_search(self):
        self.__first_provider_after_search().click()

    def click_communal_payments_link(self):
        self.__communal_payments_link().click()

    def check_label_region(self, label_text):
        self.__label_region().check_text(label_text, "Коммунальные платежи")

    def open_region_block(self):
        self.__region_block().click()

    def check_label_region_in_region_block(self, label_text):
        self.__label_region_in_region_block().check_text(label_text)

    def check_active_region_in_region_block(self, region):
        """
        Проверить, выбранный регион. Если не равен переданому, то сменить на переданный.
        """
        self.open_region_block()
        if not self.__active_region_in_region_block().get_text() == region:
            self.__filter_region().send_keys(region)
            self.__region_contains_text(region).click()
            self.open_region_block()
        self.__active_region_in_region_block().check_text(region)
        self.close_region_block()

    def close_region_block(self):
        self.__button_close_region_block().click()


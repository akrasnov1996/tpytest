from src.common.constants import TypeLocator
from src.web.WebElement.WebElement import WebElement
from src.web.TemplateWebPage.TemplateWebPage import TemplateWebPage


class Tinkoff(TemplateWebPage):
    """
    Класс описывающий страницу www.tinkoff.ru
    """
    def __init__(self, driver):
        super().__init__(driver)

from src.common.constants import TypeLocator
from src.common.exceptions.Exceptions import ElementNotFound, ElementWasFound
from src.web.WebElement.WebElement import WebElement
from src.web.TemplateWebPage.TemplateWebPage import TemplateWebPage



class ProviderPayment(TemplateWebPage):
    """
    Класс описывающий страницу https://www.tinkoff.ru/payments/kommunalnie-platezhi/zhku-moskva/
    """
    def __init__(self, driver):
        super().__init__(driver)

# ------------ * Методы для создания компонентов страницы * ------------ #

    def __second_tab(self):
        return WebElement(self.driver, '//a[@class = "ui-link ui-menu-second__link"]', TypeLocator.XPATH)

    def __payer_code(self):
        """ Из этого компонента можно получить border-top-color."""
        return WebElement(self.driver, '//div[contains(@class, "ui-input") and contains(@class, "ui-input_fade")]', TypeLocator.XPATH)

    def __payer_code_input(self):
        """ В этот компонент можно кликать, и посылать текст."""
        return WebElement(self.driver, '(//div[@class = "ui-input__column"]/input)[1]', TypeLocator.XPATH)

    def __period(self):
        return WebElement(self.driver, '(//div[contains(@class, "ui-input") and contains(@class, "ui-input_fade")])[2]', TypeLocator.XPATH)

    def __period_input(self):
        return WebElement(self.driver, '(//div[@class = "ui-input__column"]/input)[2]', TypeLocator.XPATH)

    def __sum(self):
        return WebElement(self.driver, '(//div[contains(@class, "ui-input")])[12]', TypeLocator.XPATH)

    def __sum_input(self):
        return WebElement(self.driver, '(//div[contains(@class, "ui-input")])[12]//input', TypeLocator.XPATH)

    def __payer_code_error_text(self):
        return WebElement(self.driver, "//div[contains(@class, 'ui-form-field-error-message_text')]", TypeLocator.XPATH)

    def __period_error_text(self):
        return WebElement(self.driver, "//div[contains(@class, 'ui-form-field-error-message_date')]", TypeLocator.XPATH)

    def __sum_error_text(self):
        return WebElement(self.driver, "//div[contains(@class, 'ui-form-field-error-message_amount')]", TypeLocator.XPATH)

    def __pay_button(self):
        return WebElement(self.driver, '//button[contains(@class, "ui-button_provider-pay ")]', TypeLocator.XPATH)

    def __header_banner(self):
        return WebElement(self.driver, '//div[@class = "ui-header-banner"]', TypeLocator.XPATH)

    def __fading_slide(self):
        return WebElement(self.driver, '//div[@class = "ui-fading-slide"]', TypeLocator.XPATH)

    def __menu_second(self):
        return WebElement(self.driver, "//div[contains(@class, 'ui-menu-second')]", TypeLocator.XPATH)

    def __payer_code_error_hint(self):
        return WebElement(self.driver, "//label[@for = 'payerCode']//span[contains(@class, 'ui-hint_error_icon')]/*[name()='svg']/*[name()='path']", TypeLocator.XPATH)

    def __period_error_hint(self):
        return WebElement(self.driver, "//div[contains(@class, 'ui-form__row_date')]//span[contains(@class, 'ui-hint_error_icon')]/*[name()='svg']/*[name()='path']", TypeLocator.XPATH)

    def __amount_error_hint(self):
        return WebElement(self.driver, "//div[contains(@class, 'ui-form__row_amount')]//span[contains(@class, 'ui-hint_error_icon')]/*[name()='svg']/*[name()='path']", TypeLocator.XPATH)


# ------------ * Методы для взаимодействия со страницей * ------------ #

    def get_html_provider(self):
        return self.__header_banner().get_text() + self.__fading_slide().get_text() + self.__menu_second().get_text()

    def click_pay(self):
        self.__pay_button().click()

    def open_second_menu_tab(self):
        self.__second_tab().click()

    def set_payer_code(self, keys):
        self.clear_payer_code()
        self.__payer_code_input().send_keys(keys)

    def set_period(self, keys):
        self.clear_period()
        self.__period_input().send_keys(keys)

    def set_sum(self, keys):
        self.clear_sum()
        self.__sum_input().send_keys(keys)

    def clear_payer_code(self):
        self.__payer_code_input().clear()

    def clear_period(self):
        self.__period_input().clear()

    def clear_sum(self):
        self.__sum_input().clear()

    def check_color_payer_code(self, color):
        self.__payer_code().check_color(color, "border-bottom-color")
        self.__payer_code().check_color(color, "border-top-color")
        self.__payer_code().check_color(color, "border-left-color")
        self.__payer_code().check_color(color, "border-right-color")

    def check_color_period(self, color):
        self.__period().check_color(color, "border-bottom-color")
        self.__period().check_color(color, "border-right-color")
        self.__period().check_color(color, "border-left-color")
        self.__period().check_color(color, "border-top-color")

    def check_color_sum(self, color):
        self.__sum().check_color(color, "border-bottom-color")
        self.__sum().check_color(color, "border-right-color")
        self.__sum().check_color(color, "border-top-color")
        self.__sum().check_color(color, "border-left-color")

    def payer_code_error_check_text(self, text):
        self.__payer_code_error_text().check_text(text)

    def period_error_check_text(self, text):
        self.__period_error_text().check_text(text)

    def sum_error_check_text(self, text):
        self.__sum_error_text().check_text(text, text.split(" ")[0])

    def payer_code_error_check_color(self, color):
        self.__payer_code_error_text().check_color(color)

    def period_error_check_color(self, color):
        self.__period_error_text().check_color(color)

    def sum_error_check_color(self, color):
        self.__sum_error_text().check_color(color)

    def check_color_error_hint_payer_code(self, color):
        self.__payer_code_error_hint().check_color(color, "stroke")

    def check_color_error_hint_period(self, color):
        self.__period_error_hint().check_color(color, "stroke")

    def check_color_error_hint_amount(self, color):
        self.__amount_error_hint().check_color(color, "stroke")

    def check_error_payer_code_doesnt_exists(self):
        try:
            self.__payer_code_error_text()
        except ElementNotFound:
            return
        else:
            raise ElementWasFound("Сообщение об ошибке в первом поле.")

    def check_period_error_doesnt_exist(self):
        try:
            self.__period_error_text()
        except ElementNotFound:
            return
        else:
            raise ElementWasFound("Сообщение об ошибке во втором поле.")

    def check_sum_error_doesnt_exist(self):
        try:
            self.__sum_error_text()
        except ElementNotFound:
            return
        else:
            raise ElementWasFound("Сообщение об ошибке в четвертом поле.")

    def check_error_hint_payer_code_doesnt_exists(self):
        try:
            self.__payer_code_error_hint()
        except ElementNotFound:
            return
        else:
            raise ElementWasFound("Красный крестик в первом поле.")

    def check_period_error_hint_doesnt_exist(self):
        try:
            self.__period_error_hint()
        except ElementNotFound:
            return
        else:
            raise ElementWasFound("Красный крестик во втором поле.")

    def check_sum_error_hint_doesnt_exist(self):
        try:
            self.__amount_error_hint()
        except ElementNotFound:
            return
        else:
            raise ElementWasFound("Красный крестик в четвертом поле.")
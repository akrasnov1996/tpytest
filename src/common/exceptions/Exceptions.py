class UnknowLocatorException(Exception):
    def __init__(self, *a, **kw):
        super().__init__("Неизвестный тип локатора: {}".format(a, kw))


class ElementNotFound(Exception):
    def __init__(self, locator, locator_type):
        super().__init__("Не найден элемент. С локатором: {}\n, Типом локатора: {}".format(locator, locator_type))


class ElementWasFound(Exception):
    def __init__(self, message):
        super().__init__("Найден элемент, которого не должно быть. Описание элемента: {}".format(message))


class TextDoesntMatch(Exception):
    def __init__(self, element_text, text):
        super().__init__("Текст элемента не равен переданому! Текст элемента: {},\nПолученный текст: {}".format(element_text, text))


class TextWasFound(Exception):
    def __init__(self, element_text, text):
        super().__init__("Подстрока найдена в тексте элемента! Текст элемента: {},\nПолученный текст: {}".format(element_text, text))


class ColorDoesntMatch(Exception):
    def __init__(self, element_color, color, css_property):
        super().__init__("Цвет элемента не равен переданому! Цвет элемента: {},\nПолученный цвет: {},\nCSS свойство: {}".format(element_color, color, css_property))


class DriverNotFound(Exception):
    def __init__(self, *message):
        super().__init__("Не понятно, в каком браузере запускать. driver_type: {}".format(message))


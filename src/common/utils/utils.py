from src.common.exceptions.Exceptions import TextDoesntMatch

"""
Полезные штучки, которые больше некуда положить.
"""
def compare_text(text_1, text_2):
    try:
        assert (text_1 == text_2)
    except AssertionError:
        raise TextDoesntMatch(text_1, text_2)
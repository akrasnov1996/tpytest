class TypeLocator(object):
    """
    Типы поддерживаемых локаторов для кастомных веб-элементов.
    """
    XPATH = "xpath"
    CSS = "css"
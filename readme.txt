Для запуска тестов необходимо устновить:
Python 3.5.2 (https://www.python.org/)
lxml (3.7.2)
pytest (2.9.0)
pytest-allure-adaptor (1.7.6)                                                                                           
pytest-pythonpath (0.7.1)                                                                                               
selenium (2.53.6)                                                                                                       

После установки Python'a, выполнить в консоли (/python/ и /python/Script должны быть добавлены в Path):
pip install <имя библиотеки>
Или установить все нужные библиотеки через requirements.txt:
cd /tpytest/
pip install -r requirements.txt

Или использовать готовое виртуальное окружение из репозитория. 
Для включения окружения выполнить:  
	1. cd /tpytest/tpytest-virtenv/Scripts/
	2. activate.bat

Для запуска тестов:
    1. cd /tpytest/test
    2. py.test

Платформы:
    1. win10, Chrome, Версия 56.0.2924.87 (64-bit).

Для создания отчета запускать c флагом py.test --alluredir <имя папки с отчетом>. После выполнения тестов, будет автоматически сгенерирована xml из которой нужно сгенерировать отчет.
Инструкцию для своей платформы можно найти в документации -- http://wiki.qatools.ru/display/AL/Allure+Commandline.
Пример готового отчета можно посмотреть в /tpytest/examples/allure-report.
Если не открывается сгенерированный отчет, то возможно, стоит попаться открыть его в firefox (подробнее https://github.com/allure-framework/allure1/wiki/FAQ-and-Troubleshooting#i-see-nothing-when-opening-the-report-page-in-my-browser--access-denied-message).
import pytest
import allure

from src.web.WebPages.tinkoff import Tinkoff
from src.web.WebPages.payments import Payments
from src.web.WebPages.provider_payment import ProviderPayment



@allure.feature('Feature_1')
@allure.story('Story_1')
class TestSuiteOne(object):

    @pytest.allure.severity(pytest.allure.severity_level.CRITICAL)
    @pytest.allure.testcase('WEB-UI ТЗ.docx')
    def test_payment(self, driver):

        first_region = "г. Москва"
        label_first_region = "Коммунальные платежи в Москве"
        second_region = "г. Санкт-Петербург"
        label_second_region = "Коммунальные платежи в Санкт-Петербурге"

        provider_first = "ЖКУ-Москва"
        # Сюда будет сохранен провайдер из интерфеса.
        # После проверки, что первый провайдер в списке,
        # соответвует ожидаемому (provider_first).
        provider_first_ui = ""

        normal_color = "#DDDFE0"
        full_field_color = "#EEF2F7"
        error_color = "#DD5656"

        empty_field = "Поле обязательное"
        incorrect_field = "Поле неправильно заполнено"
        # Для поля "период", другой текст.
        incorrect_field_period = "Поле заполнено некорректно"

        payer_code_valid = "1234567890"
        payer_code_invalid_list = ["6", "123", "!@#!", "123456789011"]

        valid_period = "01.2017"
        invalid_period_list = ["011", "1", "01.201"]

        valid_min_amount = "10"
        invalid_amount = ["0", "5", "9"]
        valid_max_amount = "15 000"
        invalid_amount_max = ["15001", "15002", "150010", "16001", "99999999999"]


        with pytest.allure.step('Открыть раздел "Платежи"'):
            tinkoff = Tinkoff(driver)
            tinkoff.click_payments()
            payments = Payments(driver)
        with pytest.allure.step('Открыть "Коммунальные платежи"'):
            payments.click_communal_payments_link()
        with pytest.allure.step('Проверить текущий регион на странице выбора провайдера -- должен быть "{}"'.format(first_region)):
            payments.check_label_region(label_first_region)
        with pytest.allure.step('Проверить на форме выбора региона выбранный регион (сменить, если неправильный)  -- должен быть "{}"'.format(first_region)):
            payments.check_active_region_in_region_block(first_region)
        with pytest.allure.step('Проверить, что первый провайдер -- это "{}"'.format(provider_first)):
            payments.check_first_provider(provider_first)
        with pytest.allure.step('Сохранить наименование первого провайдера'):
            provider_first_ui = payments.get_first_provider()
        with pytest.allure.step('Открыть форму платежа для первого провайдера'):
            payments.click_link_first_provider()
            provider = ProviderPayment(driver)
        with pytest.allure.step('Сохранить открытую страницу'):
            provider_html = provider.get_html_provider()
        with pytest.allure.step('Открыть вкладку "Оплата" на странице провайдера'):
            provider.open_second_menu_tab()

        with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле обязательное"'):
            provider.check_error_payer_code_doesnt_exists()
        with pytest.allure.step('Проверить, что нет подсказки на "Код плательщика"'):
            provider.check_error_hint_payer_code_doesnt_exists()
        with pytest.allure.step('Проверить, что цвет поля "Код плательшика" в норме'):
            provider.check_color_payer_code(normal_color)
        with pytest.allure.step('Нажать "Оплатить"'):
            provider.click_pay()
        with pytest.allure.step('Проверить, что для "Код плательщика" появилось сообщение об ошибке "Обязательное поле пустое"'):
            provider.payer_code_error_check_text(empty_field)
        with pytest.allure.step('Проверить, что поле "Код плательщика" подсвечено'):
            provider.check_color_payer_code(error_color)
        with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
            provider.payer_code_error_check_color(error_color)
        with pytest.allure.step('Проверить, цвет подсказки'):
            provider.check_color_error_hint_payer_code(error_color)
        with pytest.allure.step('Установить валидное значение в поле "Код плательщика" -- {}'.format(payer_code_valid)):
            provider.set_payer_code(payer_code_valid)
        with pytest.allure.step('Нажать "Оплатить"'):
            provider.click_pay()
        with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле обязательное"'):
            provider.check_error_payer_code_doesnt_exists()
        with pytest.allure.step('Проверить, что цвет поля "Код плательшика" нормализовалось'):
            provider.check_color_payer_code(full_field_color)
        with pytest.allure.step('Проверить, что нет подсказки на "Код плательщика"'):
            provider.check_error_hint_payer_code_doesnt_exists()
        with pytest.allure.step('Очистить поле "Код плательщика"'):
            provider.clear_payer_code()

        for invalid_code in payer_code_invalid_list:
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле неправильно заполнено"'):
                provider.check_error_payer_code_doesnt_exists()
            with pytest.allure.step('Проверить, что нет подсказки на "Код плательщика"'):
                provider.check_error_hint_payer_code_doesnt_exists()
            with pytest.allure.step('Проверить, что цвет поля "Код плательшика" нормальное'):
                provider.check_color_payer_code(normal_color)
            with pytest.allure.step('Установить значение "{}" в поле "Код плательщика"'.format(invalid_code)):
                provider.set_payer_code(invalid_code)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что для "Код плательщика" появилось сообщение об ошибке "Поле неправильно заполнено"'):
                provider.payer_code_error_check_text(incorrect_field)
            with pytest.allure.step('Проверить, что поле "Код плательщика" подсвечено'):
                provider.check_color_payer_code(error_color)
            with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
                provider.payer_code_error_check_color(error_color)
            with pytest.allure.step('Проверить, цвет подсказки'):
                provider.check_color_error_hint_payer_code(error_color)
            with pytest.allure.step('Установить валидное значение в поле "Код плательщика" -- {}'.format(payer_code_valid)):
                provider.set_payer_code(payer_code_valid)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле неправильно заполнено"'):
                provider.check_error_payer_code_doesnt_exists()
            with pytest.allure.step('Проверить, что цвет поля "Код плательшика" нормальное'):
                provider.check_color_payer_code(full_field_color)
            with pytest.allure.step('Проверить, что нет подсказки на "Код плательщика"'):
                provider.check_error_hint_payer_code_doesnt_exists()
            with pytest.allure.step('Очистить поле "Код плательщика"'):
                provider.clear_payer_code()

        with pytest.allure.step('Проверить, что для "Оплачиваемый период" появилось сообщение об ошибке "Обязательное поле пустое"'):
            provider.period_error_check_text(empty_field)
        with pytest.allure.step('Проверить, что поле "Оплачиваемый период" подсвечено'):
            provider.check_color_period(error_color)
        with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
            provider.period_error_check_color(error_color)
        with pytest.allure.step('Проверить, цвет подсказки'):
            provider.check_color_error_hint_period(error_color)
        with pytest.allure.step('Установить валидное значение в поле "Оплачиваемый период" -- {}'.format(valid_period)):
            provider.set_period(valid_period)
        with pytest.allure.step('Нажать "Оплатить"'):
            provider.click_pay()
        with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле обязательное" для поля "Оплачиваемый период"'):
            provider.check_period_error_doesnt_exist()
        with pytest.allure.step('Проверить, что цвет поля "Оплачиваемый период" в норме'):
            provider.check_color_period(full_field_color)
        with pytest.allure.step('Проверить, что нет подсказки на "Оплачиваемый период"'):
            provider.check_period_error_hint_doesnt_exist()
        with pytest.allure.step('Очистить поле "Оплачиваемый период"'):
            provider.clear_period()

        for invalid_period in invalid_period_list:
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле неправильно заполнено" для поля "Оплачиваемый период"'):
                provider.check_period_error_doesnt_exist()
            with pytest.allure.step('Проверить, что нет подсказки на "Оплачиваемый период"'):
                provider.check_period_error_hint_doesnt_exist()
            with pytest.allure.step('Проверить, что цвет поля "Оплачиваемый период" нормальный'):
                provider.check_color_period(normal_color)
            with pytest.allure.step('Установить значение "{}" в поле "Оплачиваемый период"'.format(invalid_period)):
                provider.set_period(invalid_period)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что для "Оплачиваемый период" появилось сообщение об ошибке "Поле неправильно заполнено"'):
                provider.period_error_check_text(incorrect_field_period)
            with pytest.allure.step('Проверить, что поле "Оплачиваемый период" подсвечено'):
                provider.check_color_period(error_color)
            with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
                provider.period_error_check_color(error_color)
            with pytest.allure.step('Проверить, цвет подсказки'):
                provider.check_color_error_hint_period(error_color)
            with pytest.allure.step('Установить валидное значение в поле "Оплачиваемый период" -- {}'.format(valid_period)):
                provider.set_period(valid_period)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле обязательное" для поля "Оплачиваемый период"'):
                provider.check_period_error_doesnt_exist()
            with pytest.allure.step('Проверить, что цвет поля "Оплачиваемый период" нормальное'):
                provider.check_color_period(full_field_color)
            with pytest.allure.step('Проверить, что нет подсказки на "Оплачиваемый период"'):
                provider.check_period_error_hint_doesnt_exist()
            with pytest.allure.step('Очистить поле "Оплачиваемый период"'):
                provider.clear_period()

        with pytest.allure.step('Проверить, что для "Сумма" появилось сообщение об ошибке "Обязательное поле пустое"'):
            provider.sum_error_check_text(empty_field)
        with pytest.allure.step('Проверить, что поле "Сумма" подсвечено'):
            provider.check_color_sum(error_color)
        with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
            provider.sum_error_check_color(error_color)
        with pytest.allure.step('Проверить, цвет подсказки'):
            provider.check_color_error_hint_amount(error_color)
        with pytest.allure.step('Установить валидное значение в поле "Сумма" -- {}'.format(valid_max_amount)):
            provider.set_sum(valid_max_amount)
        with pytest.allure.step('Нажать "Оплатить"'):
            provider.click_pay()
        with pytest.allure.step('Проверить, что нет сообщения об ошибке "Поле обязательное" для поля "Сумма"'):
            provider.check_sum_error_doesnt_exist()
        with pytest.allure.step('Проверить, что цвет поля "Сумма" нормальный'):
            provider.check_color_sum(full_field_color)
        with pytest.allure.step('Очистить поле "Сумма"'):
            provider.clear_sum()

        for invalid_min_amount in invalid_amount:
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Минимальная сумма перевода - 10 ₽" для поля "Сумма"'):
                provider.check_sum_error_doesnt_exist()
            with pytest.allure.step('Проверить, что нет подсказки на "Сумма"'):
                provider.check_sum_error_hint_doesnt_exist()
            with pytest.allure.step('Проверить, что цвет поля "Сумма" нормальное'):
                provider.check_color_sum(normal_color)
            with pytest.allure.step('Установить в поле "Сумма значение "{}"'.format(invalid_min_amount)):
                provider.set_sum(invalid_min_amount)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что для "Сумма" появилось сообщение об ошибке "Минимальная сумма перевода - 10 ₽"'):
                provider.sum_error_check_text("Минимальная сумма перевода - 10 ₽")
            with pytest.allure.step('Проверить, что поле "Сумма" подсвечено'):
                provider.check_color_sum(error_color)
            with pytest.allure.step('Проверить, цвет подсказки'):
                provider.check_color_error_hint_amount(error_color)
            with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
                provider.sum_error_check_color(error_color)
            with pytest.allure.step('Установить валидное значение в поле "Сумма" -- {}'.format(valid_min_amount)):
                provider.set_sum(valid_min_amount)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что нет сообщения об ошибке Минимальная сумма перевода - 10 ₽" для поля "Сумма"'):
                provider.check_sum_error_doesnt_exist()
            with pytest.allure.step('Проверить, что цвет поля "Сумма" нормальное'):
                provider.check_color_sum(full_field_color)
            with pytest.allure.step('Проверить, что нет подсказки на "Сумма"'):
                provider.check_sum_error_hint_doesnt_exist()
            with pytest.allure.step('Очистить поле "Сумма"'):
                provider.clear_sum()

        for invalid_max_amount in invalid_amount_max:
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Максимальная сумма перевода - 15 000 ₽" для поля "Сумма"'):
                provider.check_sum_error_doesnt_exist()
            with pytest.allure.step('Проверить, что нет подсказки на "Сумма"'):
                provider.check_sum_error_hint_doesnt_exist()
            with pytest.allure.step('Проверить, что цвет поля "Сумма" нормальное'):
                provider.check_color_sum(normal_color)
            with pytest.allure.step('Установить в поле "Сумма значение "{}"'.format(invalid_max_amount)):
                provider.set_sum(invalid_max_amount)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что для "Сумма" появилось сообщение об ошибке "Максимальная сумма перевода - 15 000 ₽"'):
                provider.sum_error_check_text("Максимальная сумма перевода - 15 000 ₽")
            with pytest.allure.step('Проверить, что поле "Сумма" подсвечено'):
                provider.check_color_sum(error_color)
            with pytest.allure.step('Проверить, цвет сообщения об ошибке'):
                provider.sum_error_check_color(error_color)
            with pytest.allure.step('Проверить, цвет подсказки'):
                provider.check_color_error_hint_amount(error_color)
            with pytest.allure.step('Установить валидное значение в поле "Сумма" -- {}'.format(valid_max_amount)):
                provider.set_sum(valid_max_amount)
            with pytest.allure.step('Нажать "Оплатить"'):
                provider.click_pay()
            with pytest.allure.step('Проверить, что нет сообщения об ошибке "Максимальная сумма перевода - 15 000 ₽" для поля "Сумма"'):
                provider.check_sum_error_doesnt_exist()
            with pytest.allure.step('Проверить, что цвет поля "Сумма" нормальное'):
                provider.check_color_sum(full_field_color)
            with pytest.allure.step('Проверить, что нет подсказки на "Сумма"'):
                provider.check_sum_error_hint_doesnt_exist()
            with pytest.allure.step('Очистить поле "Сумма"'):
                provider.clear_sum()

        with pytest.allure.step('Открыть раздел "Платежи"'):
            provider.click_payments()
            payments = Payments(driver)
        with pytest.allure.step('В строку быстрого поиска ввести {}'.format(provider_first_ui)):
            payments.enter_filter(provider_first_ui)
        with pytest.allure.step('Проверить, что первый провайдер в списке -- "{}"'.format(provider_first_ui)):
            payments.check_first_provider_after_search(provider_first_ui)
        with pytest.allure.step('Открыть первого провайдера'):
            payments.click_link_first_provider_after_search()
            provider = ProviderPayment(driver)
        with pytest.allure.step('Проверить, что открыт тот же провайдер, что и ранее в тесте'):
            provider.compare_text(provider.get_html_provider(), provider_html)

        with pytest.allure.step('Открыть раздел "Платежи"'):
            provider.click_payments()
            payments = Payments(driver)
        with pytest.allure.step('Открыть "Коммунальные платежи"'):
            payments.click_communal_payments_link()
        with pytest.allure.step('Сменить регион на {}'.format(second_region)):
            payments.check_active_region_in_region_block(second_region)
        with pytest.allure.step('Проверить, что регион сменен на {}'.format(second_region)):
            payments.check_label_region(label_second_region)
        with pytest.allure.step('Проверить, что в списке провайдеров нет {}'.format(provider_first_ui)):
            payments.uncheck_all_providers(provider_first_ui)







import pytest
import os
from selenium import webdriver
from src.common.exceptions.Exceptions import DriverNotFound


def pytest_addoption(parser):
    parser.addoption("--driver_type", action="store", default="chrome", help="выбрать драйвер: chrome или ничего")


@pytest.fixture(scope="session", autouse=True)
def driver(request):
    driver_type = request.config.getoption("--driver_type").lower()
    if driver_type == "chrome":
        web_driver = webdriver.Chrome(os.getcwd() + "/drivers/chromedriver.exe")
    else:
        raise DriverNotFound(driver_type)
    web_driver.get("https://www.tinkoff.ru/")
    web_driver.maximize_window()

    def fin():
        web_driver.close()
        web_driver.quit()

    request.addfinalizer(fin)
    return web_driver